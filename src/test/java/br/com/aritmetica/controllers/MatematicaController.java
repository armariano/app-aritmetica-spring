package br.com.aritmetica.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MatematicaController {

    @GetMapping("/")
    public String OlaMunda(){
        return "Olá mundo";
    }
}
