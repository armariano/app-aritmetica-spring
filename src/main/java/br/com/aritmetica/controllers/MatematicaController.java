package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/somar")
    public RespostaDTO somar(@RequestBody EntradaDTO entrada) {
        if (entrada.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie ao menos dois números para serem somados.");
        }
        RespostaDTO resposta = matematicaService.somar(entrada);
        return resposta;
    }

    @PutMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody EntradaDTO entrada){

        validarQtdNumero(entrada);
        validarNumeroNatural(entrada);

        RespostaDTO respostaDTO = matematicaService.subtrair(entrada);
        return respostaDTO;
    }

    @PutMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody EntradaDTO entrada){
        validarQtdNumero(entrada);
        validarNumeroNatural(entrada);
        RespostaDTO respostaDTO = matematicaService.multiplicar(entrada);
        return respostaDTO;
    }

    @PutMapping("/dividir")
    public RespostaDTO dividir(@RequestBody EntradaDTO entrada){
        validarQtdNumero(entrada);
        validarNumeroNatural(entrada);
        RespostaDTO respostaDTO = matematicaService.dividir(entrada);
        return respostaDTO;
    }

    public void validarNumeroNatural(EntradaDTO entradaDTO){
        for (int item : entradaDTO.getNumeros()){
            if(item < 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe apenas números naturais / inteiros");
            }
        }
    }

    public void validarQtdNumero(EntradaDTO entradaDTO){
        if (entradaDTO.getNumeros().size() > 2 || entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe dois números para efetuar cálculo.");
        }
    }
}
