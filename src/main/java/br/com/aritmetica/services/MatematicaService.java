package br.com.aritmetica.services;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO somar(EntradaDTO entradaDTO){

        RespostaDTO respostaDTO = new RespostaDTO();
        int numero = 0;

        for (int n: entradaDTO.getNumeros()){
            numero += n;
        }
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO subtrair(EntradaDTO entradaDTO){
        RespostaDTO respostaDTO = new RespostaDTO();
        int numero = 0;

        numero = entradaDTO.getNumeros().get(0) - entradaDTO.getNumeros().get(1);

        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO multiplicar(EntradaDTO entradaDTO){
        RespostaDTO respostaDTO = new RespostaDTO();
        int numero = 0;

        numero = entradaDTO.getNumeros().get(0) * entradaDTO.getNumeros().get(1);

        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO dividir(EntradaDTO entradaDTO){
        RespostaDTO respostaDTO = new RespostaDTO();
        int numero = 0;

        numero = entradaDTO.getNumeros().get(0) / entradaDTO.getNumeros().get(1);

        respostaDTO.setResultado(numero);
        return respostaDTO;
    }
}
